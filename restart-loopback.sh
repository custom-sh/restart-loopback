echo 'Restarting loopback server, default listening port 3000 ...'
loopback_pids=`lsof -t -i :3000`

if [ "$loopback_pids" -gt 0 ]
then
  echo 'PIDS:' $loopback_pids
  kill -KILL $loopback_pids
else
  echo 'No processes running at 3000'
fi

echo 'Trying to run loopback again. Make sure you re in the main folder'
node .